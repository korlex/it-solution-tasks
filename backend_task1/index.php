<!DOCTYPE html>
<html>
	<body>
		<form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>">
			<input type="text" name="value" placeholder="Your value">
			<input type="submit">
		</form>

		<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$val = $_POST['value']; 

			if (filter_var($val, FILTER_VALIDATE_INT) && $val != 0)
			{
				$digit = ceil(log10(abs($val) + 1));
				for($i=0; $i<=$val; $i++)
				{
					if ($i % $digit == 0) echo $i . ' ';
				}
				
			} else echo "Your variable is not an integer or equals 0. Try again.";
			
		}
		?>

	</body>
</html>