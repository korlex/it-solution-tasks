<?php
if (isset($_POST['url'])) 
{
    $url = $_POST['url'];
    if (filter_var($url, FILTER_VALIDATE_URL))
    {
        $temp1 = strpos($url,'//') + 2;
    	$result = substr($url,$temp1,strlen($url));
    	$dd=strpos($result,'/');
    	if($dd == 0)
    	{
        	$dd = strlen($result);
    	}
    	echo "<a href = $url>" . substr($result,0,$dd) . "</a>";
    } 
    else
    {
    	echo "url is not valid";
    }
    exit();
}
?>

<html>
	<head>
		<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
		<script>
			$(document).ready(function(){
				$('form').submit(function(event){
					event.preventDefault();
					$.ajax({
						type: $(this).attr('method'),
						data: $('form').serialize(),
						success: function(data) {
							$('#result').html(data);
						}
					});
				});
			});
		</script>
	</head>
	
	<body>
		<form method="POST">
			<input type="text" name="url" placeholder="Your url"/>
			<input type='submit'/>
		</form>
		<div id="result"></div>
	</body>
</html>